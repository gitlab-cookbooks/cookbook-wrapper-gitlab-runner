cookbook-wrapper-gitlab-runner Cookbook
=======================================


This cookbook is a wrapper for cookbook-gitlab-runner.

It is used to override cookbook-gitlab-runner attributes, with the attributes
that are read from the Chef Vault.

Any change that is not related to the attribute read, should be done in
cookbook-gitlab-runner


License and Authors
-------------------
Authors: Marin Jankovski
