default['cookbook-gitlab-runner']['runner_tokens'] = {}
default['cookbook-gitlab-runner']['digitalocean_tokens'] = {}
default['cookbook-gitlab-runner']['secrets']['backend'] = 'chef_vault'
default['cookbook-gitlab-runner']['secrets']['path'] = 'gitlab-runner-chatops'
default['cookbook-gitlab-runner']['secrets']['key'] = 'prd'

default['cookbook-wrapper-gitlab-runner']['use_rewriter'] = false

default['cookbook-wrapper-gitlab-runner']['systemd'] = {}
default['cookbook-wrapper-gitlab-runner']['systemd']['limits'] = {
    'LimitNOFILE' => '4096:4096',
}
default['cookbook-wrapper-gitlab-runner']['systemd']['exec_start'] = ''
default['cookbook-wrapper-gitlab-runner']['systemd']['timeout_stop_sec'] = 7200

# As per https://www.freedesktop.org/software/systemd/man/systemd.kill.html#KillMode=
# the default mode - if not configured explicitly - is `control-group`. In that case
# all processes in the process group originated at Runner process will get the configured
# kill signal immediately when stop or restart of the service is requested.
#
# In most cases we don't wont that. As we're configuring graceful shutdown
# (KillSignal=SIGQUIT in our case) we want the Runner to control shutdown procedure
# of itself and any other command that it could start, for example Docker Machine
# commands or Fleeting plugins.
#
# With KillMode=mixed we get that behavior. Kill signal is sent only to the Runner
# and Runner can gracefully terminate itself.
#
# If that would not happen and Runner process would not exit before the timeout
# defined by `timeout_stop_sec` above OR if some of the processes from the process
# group would be left behind after Runner did exit, KillMode=mixed will ensure that
# all these leftovers will receive the final kill signal (SIGKILL) at once.
default['cookbook-wrapper-gitlab-runner']['systemd']['kill_mode'] = 'mixed'

default['cookbook-wrapper-gitlab-runner']['systemd']['remove_machines_on_stop'] = false
default['cookbook-wrapper-gitlab-runner']['systemd']['remove_machines_on_stop_parallel'] = 3

default['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config'] = {}
default['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config']['enable-install-gpu-driver'] = false
default['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config']['binfmt_image'] = 'tonistiigi/binfmt:qemu-v8.1.5-43'
default['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config']['binfmt_args'] = '--install all'

default['cookbook-wrapper-gitlab-runner']['cron-chef-client-wrapper']['cron_hour'] = '*/6'
default['cookbook-wrapper-gitlab-runner']['cron-chef-client-wrapper']['cron_minute'] = '0'
default['cookbook-wrapper-gitlab-runner']['cron-chef-client-wrapper']['enable'] = false
