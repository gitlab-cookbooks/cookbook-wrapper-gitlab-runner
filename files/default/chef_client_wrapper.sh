#!/bin/bash
# Script triggered via a cronjob, by default on a 6h schedule
# Output should reside in /var/log/syslog
# Context: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15806
############################################################################
set -eo pipefail

/usr/local/bin/chef-client-is-enabled
mv /etc/nsswitch.conf /etc/nsswitch.conf.bak
trap 'mv /etc/nsswitch.conf.bak /etc/nsswitch.conf' EXIT
chef-client
