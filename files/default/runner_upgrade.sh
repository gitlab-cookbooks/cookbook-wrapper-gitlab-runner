#!/bin/bash

set -eo pipefail

function _stop_chef() {
    if sudo systemctl status chef-client >/dev/null; then
        echo Stopping chef-client...
        sudo chef-client-disable "Running GitLab Runner upgrade (which may take a long time). Will be restored after upgrade is done"
    fi
}

function _stop() {
    while sleep 20s; do
        JOBS=$(curl -s http://localhost:9402/debug/jobs/list | wc -l)
        if [[ "${JOBS}" == "0" ]]; then
            break
        fi

        echo "$(date --rfc-3339=seconds): ${JOBS} running jobs on ${HOSTNAME}..."
    done &

    echo Signal stop of build processing...
    sudo gitlab-runner stop

    echo Uninstalling gitlab-runner system service...
    sudo gitlab-runner uninstall
}

function _update() {
    sudo apt-get update

    if which docker-machine; then
        ORIGINAL_MACHINE_STORAGE_PATH="/root/.docker/machine"

        if [ -e "$ORIGINAL_MACHINE_STORAGE_PATH" ]; then
            MACHINE_TMP_DIR=$(mktemp -d)
            MACHINE_STORAGE_PATH="${MACHINE_TMP_DIR}/machine"

            echo "Cleaning up docker-machines - in temp directory ${MACHINE_STORAGE_PATH}"

            mv "${ORIGINAL_MACHINE_STORAGE_PATH}" "${MACHINE_TMP_DIR}"
            mkdir -p "${ORIGINAL_MACHINE_STORAGE_PATH}/machines"

            # Copying certs back to their place, so docker-machine will not fail on
            # concurrent create operations (known problem of "fresh" hosts without
            # generated certs)
            [ -e "${MACHINE_STORAGE_PATH}/certs" ] && cp -a "${MACHINE_STORAGE_PATH}/certs" "${ORIGINAL_MACHINE_STORAGE_PATH}" || echo "No certs directory that could be copied"

            nohup sudo -H env MACHINE_STORAGE_PATH="${MACHINE_STORAGE_PATH}" /root/remove_machines.sh >/var/log/machines-remove-on-runner-upgrade.log 2>&1 &
        fi
    fi

    echo Starting chef-client...
    sudo chef-client-enable
    sudo chef-client

    if ! sudo gitlab-runner status; then
        echo GitLab Runner is not running, starting...
        sudo gitlab-runner start
    fi


    if ! sudo gitlab-runner status; then
        echo GitLab Runner is not running...
        exit 1
    fi

    echo Verify the new version...
    gitlab-runner --version
}

function _poweroff() {
    echo "Shutting down the ${HOSTNAME} host..."
    sudo poweroff
}

function _reboot() {
    echo "Rebooting down the ${HOSTNAME} host..."
    sudo reboot
}

case "${1}" in
    stop_chef)
        _stop_chef
        ;;
    stop)
        _stop_chef
        _stop
        ;;
    stop_and_poweroff)
        _stop_chef
        _stop
        _poweroff
        ;;
    stop_and_reboot)
        _stop_chef
        _stop
        _reboot
        ;;
    update)
        _update
        ;;
    *)
        _stop_chef
        _stop
        _update
        ;;
esac
