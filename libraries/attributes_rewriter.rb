class AttributesRewriter
  MISSING_TOKEN_WARNING =
    'Define the runner token either in the role directly (not recommended) or in one of the vaults. ' +
    %q(If you're using the vault approach, make sure to set '"token": null' in the role JSON.)

  def initialize(node)
    @node = node
    @attributes = @node.default['cookbook-gitlab-runner']
  end

  def rewrite!
    prepare!
    @node.default['cookbook-gitlab-runner'] = @attributes
    @node
  end

  private

  def prepare!
    runners.each do |name, runner|
      runners[name] = prepare_runner(name, runner)
    end

    @attributes['runners'] = runners
  end

  def prepare_runner(name, runner)
    runner['global'] = prepare_runner_global(name, runner['global'])
    runner['machine'] = prepare_runner_machine(name, runner['machine']) if runner.key?('machine')

    if !runner['global']['token'] || runner['global']['token'].empty?
      Chef::Log.debug('---- start: MISSING RUNNER TOKEN ----')
      Chef::Log.warn(MISSING_TOKEN_WARNING)
      Chef::Log.debug("global section of the runner: #{runners[name]['global']}")
      Chef::Log.debug("runner_tokens from secrets (if any): #{runner_tokens}")
      Chef::Log.debug("runner is: #{name}")
      Chef::Log.debug("host is: #{host}")
      Chef::Log.debug('---- end: MISSING RUNNER TOKEN ----')
    end

    runner
  end

  def prepare_runner_global(name, global)
    global['name'] = "#{host_slug}-#{global['name']}"
    return global unless !global['token']

    global['token'] = host_runner_token(name)
    global
  end

  def prepare_runner_machine(name, machine)
    machine['MachineName'] = "#{host_slug}-#{machine['MachineName']}"
    machine['MachineOptions'].map! do |option|
      if option.match /^digitalocean-access-token=$/
        "digitalocean-access-token=#{host_runner_digitalocean_token(name)}"
      else
        option
      end
    end
    machine
  end

  def host_runner_token(runner)
    if runner_tokens.key?(host) && runner_tokens[host].key?(runner)
      return runner_tokens[host][runner]
    end

    nil
  end

  def host_runner_digitalocean_token(runner)
    if digitalocean_tokens.key?(host) && digitalocean_tokens[host].key?(runner)
      return digitalocean_tokens[host][runner]
    end

    nil
  end

  def runners
    @runners ||= @attributes['runners'] || {}
  end

  def runner_tokens
    @runner_tokens ||= @attributes['runner_tokens'] || {}
  end

  def digitalocean_tokens
    @digitalocean_tokens ||= @attributes['digitalocean_tokens'] || {}
  end

  def host
    @host ||= @node.name
  end

  def host_slug
    @host_slug ||= @node.name.gsub(/[^a-zA-Z0-9]/, '-')
  end
end
