name             'cookbook-wrapper-gitlab-runner'
maintainer       'GitLab Inc.'
maintainer_email 'marin@gitlab.com'
license          'MIT'
description      'Wrapper for cookbook-gitlab-runner'
version          '0.1.57'

depends 'gitlab-vault'
depends 'cookbook-gitlab-runner'
depends 'gitlab_secrets'
