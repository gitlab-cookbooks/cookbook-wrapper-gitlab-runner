##
# what: triggers a custom chef-client every 6 hours
# why: sometimes chef-client would fail due a known bug: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15806
#
# The chef_client_wrapper.sh code can be found here:
# https://gitlab.com/gitlab-cookbooks/cookbook-wrapper-gitlab-runner/-/blob/master/files/default/chef_client_wrapper.sh
##

cron_chef_client_wrapper = node['cookbook-wrapper-gitlab-runner']['cron-chef-client-wrapper']

cron 'chef-client wrapper' do
  action cron_chef_client_wrapper['enable'] ? :create : :delete
  command 'sudo /root/chef_client_wrapper.sh >> /var/log/syslog 2>&1'
  hour cron_chef_client_wrapper['cron_hour']
  minute cron_chef_client_wrapper['cron_minute']
  only_if { ::File.exist?('/root/chef_client_wrapper.sh') }
end
