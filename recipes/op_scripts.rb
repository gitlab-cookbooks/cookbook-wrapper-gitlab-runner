# DEPRECATED - check if anything using this is left and if not -> remove!
cookbook_file '/root/machines_operations.sh' do
  owner 'root'
  group 'root'
  mode '0755'
end

# Needed for runner_upgrade.sh script
cookbook_file '/root/remove_machines.sh' do
  owner 'root'
  group 'root'
  mode '0755'
end

# Needed to runner_upgrade.sh script on systemd
directory '/etc/systemd/system/gitlab-runner.service.d' do
  owner 'root'
  group 'root'
  mode '0755'
  recursive true
end

# Needed to runner_upgrade.sh script on systemd
template 'kill.conf' do
  path '/etc/systemd/system/gitlab-runner.service.d/kill.conf'
  source 'kill.conf.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables(
    timeout_stop_sec: node['cookbook-wrapper-gitlab-runner']['systemd']['timeout_stop_sec'],
    kill_mode: node['cookbook-wrapper-gitlab-runner']['systemd']['kill_mode']
  )
end

# Support deprecated `limits` setting - it may be used somewhere
limits = node['cookbook-wrapper-gitlab-runner']['limits'] || node['cookbook-wrapper-gitlab-runner']['systemd']['limits']

template 'limits.conf' do
  path '/etc/systemd/system/gitlab-runner.service.d/limits.conf'
  source 'limits.conf.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables(
    limits: limits
  )
end

node_systemd = node['cookbook-wrapper-gitlab-runner']['systemd']
execStartCommand = node_systemd['exec_start']

template 'systemd_exec_start.conf' do
  source 'systemd_exec_start.conf.erb'
  path '/etc/systemd/system/gitlab-runner.service.d/exec_start.conf'
  owner 'root'
  group 'root'
  mode '0644'
  variables(
    command: execStartCommand
  )
  not_if { execStartCommand == '' }
end

template 'systemd_exec_stop_post.conf' do
  source 'systemd_exec_stop_post.conf.erb'
  path '/etc/systemd/system/gitlab-runner.service.d/exec_stop_post.conf'
  owner 'root'
  group 'root'
  mode '0644'
  variables(
    remove_machines_on_stop: node_systemd['remove_machines_on_stop'],
    remove_machines_on_stop_parallel: node_systemd['remove_machines_on_stop_parallel']
  )
end

cookbook_file '/root/runner_upgrade.sh' do
  owner 'root'
  group 'root'
  mode '0755'
end

cookbook_file '/etc/logrotate.d/machines-remove-on-upgrade' do
  owner 'root'
  group 'root'
  mode '0644'
end

cookbook_file '/etc/gitlab-runner/cloud-config.conf' do
  owner 'root'
  group 'root'
  mode '0644'
end

template 'cloud-config.v2.conf' do
  source 'cloud-config.v2.conf.erb'
  path '/etc/gitlab-runner/cloud-config.v2.conf'
  owner 'root'
  group 'root'
  mode '0644'
  variables(
    enable_install_gpu_driver: node['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config']['enable-install-gpu-driver'],
    enable_custom_arm_settings: node['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config']['enable-custom-arm-settings'],
    binfmt_image: node['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config']['binfmt_image'],
    binfmt_args: node['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config']['binfmt_args']
  )
end

cookbook_file '/root/chef_client_wrapper.sh' do
  owner 'root'
  group 'root'
  mode '0755'
end
