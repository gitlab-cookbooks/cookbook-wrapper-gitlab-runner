original_attrs, wrapper_attrs =
  if node['cookbook-gitlab-runner']['chef_vault']
    include_recipe 'gitlab-vault'

    [
      GitLab::Vault.get(node, 'cookbook-gitlab-runner'),
      GitLab::Vault.get(node, 'cookbook-wrapper-gitlab-runner'),
    ]
  else
    secrets_hash = node['cookbook-gitlab-runner']['secrets']
    secrets = get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])

    [
      Chef::Mixin::DeepMerge.deep_merge(secrets['cookbook-gitlab-runner'], node['cookbook-gitlab-runner']),
      Chef::Mixin::DeepMerge.deep_merge(secrets['cookbook-wrapper-gitlab-runner'] || {}, node['cookbook-wrapper-gitlab-runner']),
    ]
  end

node.default['cookbook-gitlab-runner'] = original_attrs

cookbook_wrapper_gitlab_runner = wrapper_attrs
if cookbook_wrapper_gitlab_runner['use_rewriter']
  rewriter = AttributesRewriter.new(node)
  # rubocop:disable Lint/UselessAssignment
  node = rewriter.rewrite!
end
