require 'spec_helper'

describe 'cookbook-wrapper-gitlab-runner::op_scripts' do
  let(:chef_run) do
    ChefSpec::ServerRunner.new.converge(described_recipe)
  end

  describe 'configures limits' do
    shared_examples 'limits configuration' do
      it 'creates limits.conf file' do
        expect(chef_run).to create_template('/etc/systemd/system/gitlab-runner.service.d/limits.conf').with(
            owner: 'root',
            group: 'root',
            mode: '0644'
          )

        expect(chef_run).to render_file('/etc/systemd/system/gitlab-runner.service.d/limits.conf').with_content { |content|
          expect(content).to eq(<<~eos
            [Service]
            LimitNOFILE=20000
            LimitNPROC=20000:30000
                             eos
                               )
        }
      end
    end

    let(:chef_run) do
      ChefSpec::ServerRunner.new do |node|
        node.normal['cookbook-wrapper-gitlab-runner']['systemd']['limits'] = {
            'LimitNOFILE' => 20000,
            'LimitNPROC' => '20000:30000',
        }
      end.converge(described_recipe)
    end

    it_behaves_like 'limits configuration'

    context 'when old configuration attribute is used' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner']['limits'] = {
              'LimitNOFILE' => 20000,
              'LimitNPROC' => '20000:30000',
          }
        end.converge(described_recipe)
      end

      it_behaves_like 'limits configuration'
    end

    context 'when old and new configuration attribute is used' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner']['systemd']['limits'] = {
              'LimitNOFILE' => 10000,
              'LimitNPROC' => '10000:15000',
          }
          node.normal['cookbook-wrapper-gitlab-runner']['limits'] = {
              'LimitNOFILE' => 20000,
              'LimitNPROC' => '20000:30000',
          }
        end.converge(described_recipe)
      end

      it_behaves_like 'limits configuration'
    end
  end

  describe 'configures ExecStart override' do
    it %q(doesn't create exec_start.conf file) do
      expect(chef_run).not_to create_template('/etc/systemd/system/gitlab-runner.service.d/exec_start.conf')
    end

    context 'when systemd_exec_start_command is defined' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner']['systemd']['exec_start'] = 'start runner command'
        end.converge(described_recipe)
      end

      it 'creates exec_start.conf file' do
        expect(chef_run).to create_template('/etc/systemd/system/gitlab-runner.service.d/exec_start.conf').with(
            owner: 'root',
            group: 'root',
            mode: '0644'
          )

        expect(chef_run).to render_file('/etc/systemd/system/gitlab-runner.service.d/exec_start.conf').with_content { |content|
          expect(content).to eq(<<~eos
            [Service]
            ExecStart=
            ExecStart=start runner command
                             eos
                               )
        }
      end
    end
  end

  describe 'configures ExecStopPost override' do
    it 'creates exec_stop_post.conf file' do
      expect(chef_run).to create_template('/etc/systemd/system/gitlab-runner.service.d/exec_stop_post.conf').with(
        owner: 'root',
        group: 'root',
        mode: '0644'
      )
    end

    context 'when remove_machines_on_stop is not set' do
      it 'creates exec_stop_post.conf file with just ExecStopPost cleanup' do
        expect(chef_run).to render_file('/etc/systemd/system/gitlab-runner.service.d/exec_stop_post.conf').with_content { |content|
          expect(content).to eq(<<~eos
            [Service]
            ExecStopPost=
                             eos
                               )
        }
      end
    end

    context 'when remove_machines_on_stop is set to true' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner']['systemd']['remove_machines_on_stop'] = true
        end.converge(described_recipe)
      end

      it 'creates exec_stop_post.conf file with machines removal call' do
        expect(chef_run).to render_file('/etc/systemd/system/gitlab-runner.service.d/exec_stop_post.conf').with_content { |content|
          expect(content).to eq(<<~eos
            [Service]
            ExecStopPost=
            ExecStopPost=/root/remove_machines.sh 3
                             eos
                               )
        }
      end

      context 'when remove_machines_on_stop_parallel is defined' do
        let(:chef_run) do
          ChefSpec::ServerRunner.new do |node|
            node.normal['cookbook-wrapper-gitlab-runner']['systemd']['remove_machines_on_stop'] = true
            node.normal['cookbook-wrapper-gitlab-runner']['systemd']['remove_machines_on_stop_parallel'] = 10
          end.converge(described_recipe)
        end

        it 'creates exec_stop_post.conf file with machines removal call and specific parameter' do
          expect(chef_run).to render_file('/etc/systemd/system/gitlab-runner.service.d/exec_stop_post.conf').with_content { |content|
            expect(content).to eq(<<~eos
            [Service]
            ExecStopPost=
            ExecStopPost=/root/remove_machines.sh 10
                               eos
                                 )
          }
        end
      end
    end
  end

  describe 'configures process kill overrides' do
    let(:expected_timeout_stop_sec) { 7200 }
    let(:expected_kill_mode) { 'mixed' }

    shared_examples 'kill configuration' do
      it 'creates kill.conf file' do
        expect(chef_run).to create_template('/etc/systemd/system/gitlab-runner.service.d/kill.conf').with(
          owner: 'root',
          group: 'root',
          mode: '0644'
        )

        expect(chef_run).to render_file('/etc/systemd/system/gitlab-runner.service.d/kill.conf').with_content { |content|
          expect(content).to eq(<<~eos
            [Service]
            TimeoutStopSec=#{expected_timeout_stop_sec}
            KillSignal=SIGQUIT
            KillMode=#{expected_kill_mode}
                             eos
                               )
        }
      end
    end

    context 'when no timeout_stop_sec is provided' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new.converge(described_recipe)
      end

      it_behaves_like 'kill configuration'
    end

    context 'when custom timeout_stop_sec value is provided' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner']['systemd']['timeout_stop_sec'] = 1234
        end.converge(described_recipe)
      end
      let(:expected_timeout_stop_sec) { 1234 }

      it_behaves_like 'kill configuration'
    end

    context 'when custom kill_mode value is provided' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner']['systemd']['kill_mode'] = 'control-group'
        end.converge(described_recipe)
      end
      let(:expected_kill_mode) { 'control-group' }

      it_behaves_like 'kill configuration'
    end
  end

  describe 'writes cloud-config files' do
    it 'writes cloud-config.conf' do
      expect(chef_run).to create_cookbook_file('/etc/gitlab-runner/cloud-config.conf').with(
        owner: 'root',
        group: 'root',
        mode: '0644'
      )

      expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.conf').with_content(%r{^- path: /etc/docker/daemon.json$})
      expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.conf').with_content(%r{^- path: /etc/systemd/system/docker.service.d/20-run-binfmt-container.conf$})
      expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.conf').with_content(%r{ExecStartPost=docker run --rm --privileged linuxkit/binfmt:v0.8})
      expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.conf').with_content(/^- systemctl start iptables-restore.service$/)
      expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.conf').with_content(/^- docker stop gitlab-runner-docker-cleanup$/)
    end

    shared_examples 'cloud-config.v2.conf writer' do
      it 'writes cloud-config.v2.conf' do
        expect(chef_run).to create_template('/etc/gitlab-runner/cloud-config.v2.conf').with(
          owner: 'root',
          group: 'root',
          mode: '0644'
        )

        expect(chef_run).not_to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(%r{^- path: /etc/docker/daemon.json$})
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(%r{^- path: /etc/systemd/system/docker.service.d/20-run-binfmt-container.conf$})
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(%r{ExecStartPost=docker run --rm --privileged tonistiigi/binfmt:qemu-v8.1.5-43 --install})
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(%r{^- path: /etc/systemd/system/docker.service.d/05-iptables-restore-wants.conf$})
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(%r{^- path: /etc/systemd/system.conf$})
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(%r{^- path: /var/lib/cloud/scripts/per-boot/00-enable-swap$})
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(%r{^- path: /var/lib/cloud/scripts/per-boot/01-configure-custom-sysctl$})
        expect(chef_run).not_to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(/^- systemctl start iptables-restore.service$/)
        expect(chef_run).not_to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(/^- docker stop gitlab-runner-docker-cleanup$/)
      end
    end

    it_behaves_like 'cloud-config.v2.conf writer'

    context 'when custom binfmt_image is used' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config'] = {
            'binfmt_image' => 'linuxkit/binfmt:v0.8',
            'binfmt_args' => '',
          }
        end.converge(described_recipe)
      end

      it 'uses the custom image' do
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(%r{ExecStartPost=docker run --rm --privileged linuxkit/binfmt:v0.8})
      end
    end

    context 'when install-gpu-driver is enabled' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config'] = {
            'enable-install-gpu-driver' => true,
          }
        end.converge(described_recipe)
      end

      it_behaves_like 'cloud-config.v2.conf writer'

      it 'writes gpu-driver installation part' do
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(%r{^- path: /etc/systemd/system/gpu-driver.service$})
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(/^- systemctl start gpu-driver.service$/)
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf').with_content(/^- systemctl enable gpu-driver.service$/)
      end
    end

    context 'when custom-arm-settings is enabled' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner']['ephemeral-vm-cloud-config'] = {
            'enable-custom-arm-settings' => true,
          }
        end.converge(described_recipe)
      end

      it 'renders the file without the binfmt configs when ARM custom settings are enabled' do
        expect(chef_run).to render_file('/etc/gitlab-runner/cloud-config.v2.conf')
          .with_content(%r{^(?!.*- path: /etc/systemd/system/docker\.service\.d/20-run-binfmt-container\.conf)})
      end
    end
  end
end
