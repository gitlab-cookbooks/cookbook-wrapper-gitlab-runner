require 'spec_helper'
require 'chef-vault'

describe 'cookbook-wrapper-gitlab-runner::prepare_attributes' do
  describe 'configures secrets' do
    let(:concurrent) { 10 }
    let(:sentry_dsn) { 'sentry DSN' }
    let(:node_name) { 'chefspec' }
    let(:runner_1_key) { 'runner_1' }
    let(:runner_2_key) { 'runner_2' }
    let(:runner_1_token) { 'runner_1_token' }
    let(:runner_2_token) { 'runner_2_token' }

    let(:cookbook_wrapper_gitlab_runner_attr) do
      {
        'use_rewriter' => true,
      }
    end

    let(:base_cookbook_gitlab_runner_attr) do
      {
        'global_config' => {
          'concurrent' => 10,
        },
        'runners' => {
          runner_1_key => {
            'global' => {
              'name' => 'name of the first runner',
            },
          },
          runner_2_key => {
            'global' => {
              'name' => 'name of the second runner',
            },
          },
        },
      }
    end

    let(:secrets) do
      {
        'id' => 'id',
        'cookbook-gitlab-runner' => {
          'global_config' => {
            'sentry_dsn' => sentry_dsn,
          },
          'runner_tokens' => {
            node_name => {
              runner_1_key => runner_1_token,
              runner_2_key => runner_2_token,
            },
          },
        },
      }
    end

    let(:cookbook_gitlab_runner_attr) { base_cookbook_gitlab_runner_attr }

    subject { chef_run.node.default['cookbook-gitlab-runner'] }

    shared_examples 'attributes preparation' do
      context 'when token in role is left undefined' do
        it 'fails to prepare attributes' do
          expect(Chef::Log).to receive(:warn).with(AttributesRewriter::MISSING_TOKEN_WARNING).at_least(:once)
          check_attributes(subject, {}, {})
        end
      end

      context 'when token in role is set to empty string' do
        let!(:cookbook_gitlab_runner_attr) do
          attr = base_cookbook_gitlab_runner_attr
          attr['runners'][runner_1_key]['global']['token'] = ''
          attr['runners'][runner_2_key]['global']['token'] = ''
          attr
        end

        it 'fails to prepare attributes' do
          expect(Chef::Log).to receive(:warn).with(AttributesRewriter::MISSING_TOKEN_WARNING).at_least(:once)
          check_attributes(subject, '', '')
        end
      end

      context 'when token in role is set to nil' do
        let!(:cookbook_gitlab_runner_attr) do
          attr = base_cookbook_gitlab_runner_attr
          attr['runners'][runner_1_key]['global']['token'] = nil
          attr['runners'][runner_2_key]['global']['token'] = nil
          attr
        end

        it 'prepares attributes' do
          check_attributes(subject, runner_1_token, runner_2_token)
        end
      end

      def check_attributes(attr, expected_runner_1_token, expected_runner_2_token)
        expect(attr).to have_key('global_config')
        global_config = attr['global_config']
        expect(global_config).to have_key('concurrent')
        expect(global_config['concurrent']).to eq(concurrent)
        expect(global_config).to have_key('sentry_dsn')
        expect(global_config['sentry_dsn']).to eq(sentry_dsn)

        expect(attr).to have_key('runners')
        runners = attr['runners']

        expect(runners).to have_key(runner_1_key)
        runner1 = runners[runner_1_key]
        expect(runner1).to have_key('global')
        runner1_global = runner1['global']
        expect(runner1_global).to have_key('token')
        expect(runner1_global['token']).to eq(expected_runner_1_token)

        expect(runners).to have_key(runner_2_key)
        runner2 = runners[runner_2_key]
        expect(runner2).to have_key('global')
        runner2_global = runner2['global']
        expect(runner2_global).to have_key('token')
        expect(runner2_global['token']).to eq(expected_runner_2_token)
      end
    end

    context 'when gkms vault is used' do
      let(:secret_path) { { 'path' => 'path/to', 'item' => 'item' } }
      let(:secret_key) { { 'ring' => 'ring', 'key' => 'key', 'location' => 'location' } }

      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner'] = cookbook_wrapper_gitlab_runner_attr

          attr = cookbook_gitlab_runner_attr
          attr['secrets'] = {
            'backend' => 'gkms',
            'path' => secret_path,
            'key' => secret_key,
          }
          node.normal['cookbook-gitlab-runner'] = attr
        end.converge(described_recipe)
      end

      before do
        expect_any_instance_of(Chef::Recipe).to receive(:get_secrets)
          .with('gkms', secret_path, secret_key)
          .and_return(secrets)
      end

      it_behaves_like 'attributes preparation'
    end

    context 'when chef vault is used' do
      let(:chef_run) do
        ChefSpec::ServerRunner.new do |node|
          node.normal['cookbook-wrapper-gitlab-runner'] = cookbook_wrapper_gitlab_runner_attr

          attr = cookbook_gitlab_runner_attr
          attr['chef_vault'] = node_name
          node.normal['cookbook-gitlab-runner'] = attr
        end.converge(described_recipe)
      end

      before do
        expect(::ChefVault::Item).to receive(:load).with(node_name, '_default').and_return(secrets)
      end

      it_behaves_like 'attributes preparation'
    end
  end
end
