# frozen_string_literal: true

# Cookbook:: gitlab-server
# Spec:: cron_chef_client_wrapper

require 'spec_helper'

describe 'cookbook-wrapper-gitlab-runner::cron-chef-client-wrapper' do
  context 'When the cron is enabled' do
    before do
      allow(File).to receive(:exist?).and_call_original
      allow(File).to receive(:exist?).with('/root/chef_client_wrapper.sh').and_return(true)
    end

    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '20.04') do |node, _server|
        node.normal['cookbook-wrapper-gitlab-runner']['cron-chef-client-wrapper']['enable'] = true
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the cron job' do
      expect(chef_run).to create_cron('chef-client wrapper').with(
        command: 'sudo /root/chef_client_wrapper.sh >> /var/log/syslog 2>&1',
        hour: '*/6',
        minute: '0'
      )
    end
  end

  context 'When the cron is disabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '20.04') do |node, _server|
      end.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'does not create the cron job' do
      expect(chef_run).to_not create_cron('chef-client wrapper')
    end
  end
end
